## Freiburg Thesis package

This is a LaTeX package for Bachelor, Master and PhD Theses at the University of Freiburg.

You can move all files to your `texmf` path (typically `/texmf/tex/latex/`) and run

~~~~~
texhash
~~~~~

to have LaTeX discover the new file. Alternatively, you can just copy all the files to the directory of your thesis. Then you can use it with

~~~~~
\documentclass{freiburgthesis}
~~~~~
