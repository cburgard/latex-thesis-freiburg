\ProvidesClass{freiburgthesis}
\LoadClass[twoside,a4paper,12pt]{scrbook}

\let\iint\undefined
\let\iiint\undefined
\let\iiiint\undefined

\RequirePackage{fancyhdr}
\RequirePackage{etoolbox}
\RequirePackage{amssymb,amsmath}
\RequirePackage{fncychap}
\RequirePackage{mathrsfs}
\RequirePackage{setspace}
\RequirePackage{cite}
\RequirePackage{caption}
\RequirePackage{subcaption}
\RequirePackage{floatrow}
\RequirePackage{type1cm}
\RequirePackage{array}
\RequirePackage{mathtools}
\RequirePackage{slashed}
\RequirePackage{nicefrac}
\RequirePackage{rotating} 
\RequirePackage{standalone}
\RequirePackage{xspace}
\RequirePackage{verbatim}
\RequirePackage{titlesec}
\RequirePackage{listings}
\RequirePackage{enumitem}
\RequirePackage{multirow}
\RequirePackage{hyperref}
\RequirePackage{pgfplots}
\RequirePackage{flafter}
\PassOptionsToPackage{subsection}{extraplaceins}
\RequirePackage{extraplaceins}

\floatsetup[figure]{capposition=bottom}
\floatsetup[table]{capposition=top}

\RequirePackage{environ}

\environfinalcode{}
\NewEnviron{xsubcaption}[1][-1sp]
 {\let\xsc@theoptcaption\@empty
  \let\xsc@thecaption\@empty
  \let\xsc@thelabel\@empty
  \expandafter\xsc@checkcaption\BODY\caption\xsc@checkcaption
  \expandafter\xsc@checklabel\BODY\label\xsc@checklabel
  \begingroup\edef\x{\endgroup
    \noexpand\subcaptionbox
      \ifx\xsc@theoptcaption\@empty\else
        [\unexpanded\expandafter{\xsc@theoptcaption}]%
      \fi
      {\unexpanded\expandafter{\xsc@thecaption}%
       \ifx\xsc@thelabel\@empty\else
         \noexpand\label{\unexpanded\expandafter{\xsc@thelabel}}%
       \fi} 
      \ifdim#1=-1sp % no optional argument
      \else
        [#1]%
      \fi
      {\unexpanded{\renewcommand\caption[2][]{}\renewcommand{\label}[1]{}}%
       \unexpanded\expandafter{\BODY}}}\x
}

\long\def\xsc@checkcaption#1\caption#2\xsc@checkcaption{%
  \if\relax\detokenize{#2}\relax
    \expandafter\@gobble
  \else
    \expandafter\@firstofone
  \fi
  {\expandafter\xsc@getcaption\BODY\xsc@getcaption}%
}
\long\def\xsc@getcaption#1\caption#2#3\xsc@getcaption{%
  \if[\detokenize{#2}%
    \expandafter\@firstoftwo
  \else
    \expandafter\@secondoftwo
  \fi
  {\expandafter\xsc@getcaptionopt\BODY\xsc@getcaptionopt}%
  {\def\xsc@thecaption{#2}}%
}
\long\def\xsc@getcaptionopt#1\caption[#2]#3#4\xsc@getcaptionopt{%
  \def\xsc@theoptcaption{#2}%
  \def\xsc@thecaption{#3}%
}
\long\def\xsc@checklabel#1\label#2\xsc@checklabel{%
  \if\relax\detokenize{#2}\relax
    \expandafter\@gobble
  \else
    \expandafter\@firstofone
  \fi
  {\expandafter\xsc@getlabel\BODY\xsc@getlabel}%
}
\long\def\xsc@getlabel#1\label#2#3\xsc@getlabel{%
  \def\xsc@thelabel{#2}%
}


\hypersetup{
  pdfsubject  = {Particle Physics},
  pdfkeywords = {High-Energy Physics, Particle Physics},
  pdfcreator  = {LaTeX with hyperref package},
  pdfproducer = {pdflatex},
  pdfpagemode=UseOutlines, 
  bookmarksopen=true,
  bookmarksopenlevel={0},
  bookmarksnumbered=true,
  hidelinks
}


\pgfplotsset{compat=1.7}

\captionsetup{%
  format=hang,%
  font=small,%
  labelfont={sf,bf},%
  aboveskip=0.2\normalbaselineskip,%
  position=top
}
\DeclareCaptionSubType[alph]{figure}
%\captionsetup[subfigure]{labelformat=brace,justification=centerlast}


\providecommand\DeclareUnicodeCharacter[2]{}



%---------------------------------------------------------------------------------------------------
% referencing
%---------------------------------------------------------------------------------------------------

\let\citep\cite
\newcommand\citeref[1]{Ref.\cite{#1}}
\newcommand\silentcite[1]{\makeatletter{\csname @fileswfalse\endcsname\cite{#1}}\makeatother\AtEndDocument{\nocite{#1}}}
\newcommand\silentciteref[1]{Ref.\makeatletter{\csname @fileswfalse\endcsname\cite{#1}}\makeatother\AtEndDocument{\nocite{#1}}}
\newcommand\eqlabel[1]{\label{eq:#1}}
\newcommand\secref[1]{Section~\ref{sec:#1}}
\newcommand\Secref[1]{Section~\ref{sec:#1}}
\newcommand\chapref[1]{Chapter~\ref{chap:#1}}
\newcommand\Chapref[1]{Chapter~\ref{chap:#1}}
\newcommand\appref[1]{Appendix~\ref{#1}}
\newcommand\Appref[1]{Appendix~\ref{#1}}
\renewcommand\eqref[1]{Eq.\,\ref{eq:#1}}
\newcommand\Eqref[1]{Equation~\ref{eq:#1}}
\newcommand\figref[1]{Fig.\,\ref{fig:#1}}
\newcommand\Figref[1]{Figure~\ref{fig:#1}}
\newcommand\tabref[1]{Table~\ref{tab:#1}}
\newcommand\Tabref[1]{Table~\ref{tab:#1}}


%---------------------------------------------------------------------------------------------------
% lengths and dimensions
%---------------------------------------------------------------------------------------------------

\setlength{\parskip}{3pt plus 6pt minus 2pt}
\setlength{\parfillskip}{0pt plus\dimexpr.8\textwidth}
\widowpenalty=10000
\clubpenalty=10000
\displaywidowpenalty=1000
\postdisplaypenalty=100
\allowdisplaybreaks
\raggedbottom


%\titleformat{\chapter}[display]{\sffamily\huge\bfseries}{\chaptertitlename\ \thechapter}{20pt}{\Huge}
\titleformat{\section}{\sffamily\Large\bfseries}{\thesection}{1em}{}
\titleformat{\subsection}{\sffamily\large\bfseries}{\thesubsection}{1em}{}
\titleformat{\subsubsection}{\sffamily\normalsize\bfseries}{\thesubsubsection}{1em}{}
\titleformat{\paragraph}[runin]{\sffamily\normalsize\bfseries}{\theparagraph}{1em}{}
\titleformat{\subparagraph}[runin]{\sffamily\normalsize\bfseries}{\thesubparagraph}{1em}{}

\titlespacing*{\section}{0pt}{2em plus 4em minus 1em}{0.5em plus 0.5em minus 0.5em}
\titlespacing*{\subsection}{0pt}{1em plus 1em minus .5em}{0.2em plus 0.5em}
\titlespacing*{\subsubsection}{0pt}{0.5em plus 0.5em minus 0.2em}{0em plus 0.2em}

%---------------------------------------------------------------------------------------------------
% chapter openings
%---------------------------------------------------------------------------------------------------
\newcommand\openany{\makeatletter\@openrightfalse\makeatother}
\newcommand\openright{\makeatletter\@openrighttrue\makeatother}

%---------------------------------------------------------------------------------------------------
% headings
%---------------------------------------------------------------------------------------------------

\ChNameVar{\huge}
\ChNumVar{\Huge}
\ChTitleVar{\huge\sffamily\bfseries}
\ChRuleWidth{0.6pt}
\ChNameUpperCase

\renewcommand{\DOCH}{
	% do not print ``chapter'' line
}

\renewcommand{\DOTI}[1]{
	\vspace{-2.5cm}
	\sffamily \CTV\raggedleft\mghrulefill{\RW}\par\nobreak
	\vskip 5\p@
	\CNoV\raisebox{-2pt}{\thechapter} \hfill \CTV\FmTi{#1}\\[-1ex]\nobreak
	\mghrulefill{\RW}\par\nobreak
	\vskip 35\p@
}

\renewcommand{\DOTIS}[1]{
	\vspace{-2.5cm}
	\sffamily \CTV\raggedleft\mghrulefill{\RW}\par\nobreak
	\vskip 5\p@
	\hfill \CTV\FmTi{#1}\\[-1ex]\nobreak
	\mghrulefill{\RW}\par\nobreak
	%\vskip 35\p@
}

\newcommand\dochapterhead{
	\chapterheadendvskip
	\global\@topnum\z@
	\@afterindentfalse
	\secdef\@chapter\@schapter
}	

%---------------------------------------------------------------------------------------------------
% header & footer
%---------------------------------------------------------------------------------------------------

\pagestyle{fancy}

\providecommand\chaptermark[1]{}

\renewcommand{\chaptermark}[1]{\markboth{#1}{}}
\renewcommand{\sectionmark}[1]{\markright{#1}{}}
\lhead[\fancyplain{}{\thepage}]
	{\fancyplain{}{\let\boldmath\relax\let\boldsymbol\relax\scshape\rightmark}}
\rhead[\fancyplain{}{\let\boldmath\relax\let\boldsymbol\relax\hspace{15ex}{\scshape\leftmark}}]
	{\fancyplain{}{\thepage}}
\cfoot{}
\addtolength{\headheight}{3pt}

% ---------------------------------------------------------------------------------------------------
% controlling floats
% ---------------------------------------------------------------------------------------------------

\renewcommand{\topfraction}{0.95}
\renewcommand{\bottomfraction}{0.95}
\renewcommand{\textfraction}{0.10}
\renewcommand{\floatpagefraction}{0.75}
\setcounter{totalnumber}{5}

% ---------------------------------------------------------------------------------------------------
% helper command
% ---------------------------------------------------------------------------------------------------

\newcommand{\HRule}{\rule{\linewidth}{0.6mm}} 
\newcommand{\htarget}[1]{\raisebox{\ht\strutbox}{\hypertarget{#1}{}}}
\newcommand{\clearemptydoublepage}{\clearpage\newpage\thispagestyle{empty}\cleardoublepage}
\newcommand{\emath}[1]{\ifthenelse{\equal{\f@series}{bx}\and\not\boolean{mmode}}{\mathversion{bold}\ensuremath{#1}\mathversion{normal}}{\ensuremath{#1}}}
\let\obrace\{
\let\cbrace\}


% ---------------------------------------------------------------------------------------------------
% headless toc
% ---------------------------------------------------------------------------------------------------

\gdef\tableofcontents{\makeatletter\@starttoc{toc}\makeatother}

%---------------------------------------------------------------------------------------------------
% title page
%---------------------------------------------------------------------------------------------------

\gdef\maketitle{
  \input{freiburgthesis-titlepage}
}
\newcommand\titlefontsize{\huge}
\newcommand\titlefrontbody{}
\newcommand\titlebackbody{}
\newcommand\titlebackfoot{}
\newcommand\titlebackhead{}

%---------------------------------------------------------------------------------------------------
% suppressed citations
%---------------------------------------------------------------------------------------------------

